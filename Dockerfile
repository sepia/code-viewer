# syntax=docker/dockerfile:1

# shared arguments
ARG app_abbrev
ARG app_version
ARG build_dir=/build/


FROM debian:buster-slim AS builder

# use bash for better debugging capabilities, e.g. pipefail support
SHELL [ "/bin/bash", "-c" ]

# hadolint ignore=DL3008
RUN apt-get update \
  && apt-get install \
    --yes \
    --no-install-recommends \
    ca-certificates \
    wget \
    # envsubst
    gettext-base \
    zip

# packages declares no dependencies

ARG build_dir
WORKDIR ${build_dir}
COPY src/ .
COPY substitutions.txt .

ARG app_abbrev
ARG app_title
ARG app_version
# build ARGs variable substitutions to certain files
# hadolint ignore=SC2002,DL4006
RUN set -o pipefail; \
  cat substitutions.txt \
  # build sed script commands from variable names
  # hack: use a custom delimiter (^) for sed to allow "//" in url substitutions
  | xargs -I[] echo "s^@[]@^\${[]}^g" \
  # substitute build ARGs with their values
  | envsubst \
  # run the sed script commands on the files
  | xargs -I[] sed -i [] expath-pkg.xml repo.xml \
  # finally, prevent the inclusion of the substutions file into the archive
  && rm substitutions.txt

# zip it
RUN zip -r ${app_abbrev}-${app_version}.xar ./*


FROM docker.gitlab.gwdg.de/sepia/existdb-images/existdb:5.2.0-6e8c51a0-debug

ARG app_abbrev
ARG app_version
ARG build_dir
COPY --from=builder ${build_dir}${app_abbrev}-${app_version}.xar /exist/autodeploy
