# [2.0.0](https://gitlab.gwdg.de/sepia/code-viewer/compare/v1.3.1...v2.0.0) (2021-07-05)


### Features

* **library:** transform package into a library ([89241df](https://gitlab.gwdg.de/sepia/code-viewer/commit/89241dfe70d6f946ab406834874ded6fa1fa7a5d)), closes [#1](https://gitlab.gwdg.de/sepia/code-viewer/issues/1)


### BREAKING CHANGES

* **library:** namespace changed to `http://sepia.io/code-viewer/main`

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Released]

### [1.3.1] - 2020-05-27

#### Changed
- README update


### [1.3.0] - 2020-05-26

#### Added
- GitLab templates for issues and merge requests

#### Changed
- Update to eXist-db 5.2.0


## [Unreleased]

### [1.2.0] - 2020-05-25
#### Added
- A sample file and a sample CSS to demonstrate the function of this module
- Function code-view:sample() for creating this sample output

#### Changed
- Moved CHANGELOG to this document instead of repo.xml to increase visibility of the file

#### Removed
- Removed the XHTML namespace from output
