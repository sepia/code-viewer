xquery version "3.1";

(:~
 : This module provides a syntax highlighting of XML or HTML completely
 : without JavaScript. For this purpose a given XML or HTML document or fragment
 : is serialized as string and enhanced with span classes that are completely
 : compatible with highlight.js.
 :
 : @author Mathias Göbel
 : @author Stefan Hynek
 : @author Michelle Weidling
 : @see https://highlightjs.org/
 :)

module namespace code-viewer="http://sepia.io/code-viewer/main";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xhtml="http://www.w3.org/1999/xhtml/";

(:~
 : Provides an enhanced serialization from any node to XHTML.
 :
 : @author Mathias Göbel
 : @param $node XML fragment to transform
 : @param $comments true() to include comments, false() to exclude comments
 : @param $prefix a custom prefix to prepend to class names including the delimiter. default is «hljs-»
 : @param $trim-indention true(): restart indention, false(): keep indention
 : @return pre containing the provided code serialized as string with syntax highlighting
 :)
declare function code-viewer:main($node as node(), $comments as xs:boolean?,
$prefix as xs:string?, $trim-indention as xs:boolean?) as element(pre) {
    let $prefix := if($prefix = ()) then "hljs-" else $prefix
    let $node := if($trim-indention) then serialize($node) => parse-xml() else $node
    return
      element pre {
        element code {
          if($prefix = "hljs-")
          then
            attribute class {"xml hljs"}
          else
              (),
          code-viewer:serialize($node, $comments, $prefix, count($node/ancestor::*))
        }
      }
};


(:~
 : Provides  basic serialization from any node to XHTML. It automatically
 : enables comment nodes and uses the "hljs" prefix for class names and no changes
 : to the indention
 :
 : @author Mathias Göbel
 : @param $nodes nodes to transform
 : @return pre containing the provided code serialized as string with syntax highlighting
 :)
declare function code-viewer:main($node as node())
as element(pre) {
    code-viewer:main($node, true(), "hljs-", true())
};

(:~
 : Adds syntax highlighting for elements.
 :
 : @author Michelle Weidling
 : @author Mathias Göbel
 : @param $nodes the nodes of a given XML or HTML code
 : @param $comments true() if comments should be displayed
 : @param $prefix a custom prefix to prepend to class names, default is hljs
 : @return span containing the serialized XML or HTML code, nothing for
 : comments in case they are disabled, an error string for everything that's not
 : an element, comment, or text node
 :)
declare function code-viewer:serialize($nodes as node()*, $comments as xs:boolean,
$prefix as xs:string, $trim-indention as xs:int) as node()* {
  for $node in $nodes return
    typeswitch($node)
      case document-node() return
        code-viewer:serialize($node/node(), $comments, $prefix, $trim-indention)

      case element() return (
        element span {
            attribute class {$prefix || "tag"},
            "<",
            element span {
                attribute class {$prefix || "name"},
                $node/local-name()
            },
            code-viewer:attributes($node, $prefix),
            if(not($node/node()))
                then
                    "/>"
                else
                    ">"
        },
        code-viewer:serialize($node/node(), $comments, $prefix, $trim-indention),
        if($node/node())
          then
            element span {
              attribute class {$prefix || "tag"},
              "</",
              element span {
                attribute class {$prefix || "name"},
                $node/local-name()
              },
              ">"
            }
        else ()
      )

      case comment() return
          if($comments)
          then
              element span {
                  attribute class {$prefix || "comment"},
                  serialize($node)
              }
          else
              ()

      case text() return
        if(matches($node, "^\s*\n\s+$"))
        then
            if($node/following::text())
            then
              let $num-of-spaces := (count($node/ancestor::*)
                                    - (if($node/following-sibling::node()) then 0 else 1))
              return
                text { "&#10;" ||
                 (for $i in 1 to $num-of-spaces
                  return "  ") => string-join() }
            else text { "&#10;" }
        else $node

      default return
          "unsupported"
};

(:~
 : Adds syntax highlighting for attributes.
 :
 : @author Michelle Weidling
 : @author Mathias Göbel
 : @param $element the current element
 : @param $prefix a custom class prefix if provided in the main function or "hljs" as default
 : @return the highlighted attribute
 :)
declare function code-viewer:attributes($element as element(), $prefix as xs:string)
as node()* {
  for $attr in $element/@*
  return (
    text {" "},
    element span {
      attribute class {$prefix || "attr"},
      $attr/name()
    },
    text {"="},
    element span {
      attribute class {$prefix || "string"},
      text {"""" || string($attr) || """"}
    }
  )
};
