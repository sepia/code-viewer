# Code Viewer

This app serves to display XML or (X)HTML code with syntax highlighting on a website.
It is fully compatible with any CSS themes provided by [highlight.js](https://highlightjs.org/) but without the client-sided JavaScript rendering that leads to performance losses while displaying large files.

Its main purposes are to

1. remove the dependency on JavaScript and by that providing more security
2. take load away from the client and let the server do all the work

This module has originally been developed for the DFG-funded project [Bibliothek der Neologie](https://www.bdn-edition.de) and spun off as a generic app for [SADE](https://gitlab.gwdg.de/SADE).

## Prerequisites

* ant 1.10 or higher
* eXist-db 4.2.0 or higher

## Installation

### Manual Installation

To manually install the package simply use the eXist-db package manager.
Create a `XAR` file by calling

```bash
ant
```

The resulting archive will be stored in `build/` and can be installed in eXist-db's package manager via drag and drop.

The module's functions are available after including the module in your XQuery via import:

```xml
import module namespace code-view="http://bdn-edition.de/ns/code-view";
```

This module provides options for a **basic** and **extended** mode.

### Installation As Part Of SADE

The Code Viewer is **not** shipped with SADE by default.
To install this app, please make sure you set the parameter `code-view.install` in your project's `$project.build.properties` to `true`.
Confer the ["Bibliothek der Neologie" build config](https://gitlab.gwdg.de/bibliothek-der-neologie/bdn-build/-/blob/develop/build.dev.properties) for usage.

## Usage

### Basic Mode

The basic mode offers you

* the highlighted code in an `pre` element
* full compatibility with [highlight.js](https://highlightjs.org/)'s CSS
* display of comments

Simply call

```xml
code-view:main($nodes as node()+)
```

with `$nodes` being the XML nodes you want to render, returning a `pre` element with the serialized nodes.

### Extended Mode

The extended mode offers you

* the highlighted code in an `pre` element
* the option to decide on whether you want comments to be displayed or not
* full compatibility with [highlight.js](https://highlightjs.org/)'s CSS **OR** a custom prefix for other CSS classes

Simply call

```xml
code-view:main($nodes as node()+, $comments as xs:boolean?, $prefix as xs:string?)
```

Set `$comments` to `true()` if you want comments to be displayed.
In case you pass an empty string to `$prefix` it defaults to `hljs-` which is the prefix compatible with [highlight.js](https://highlightjs.org/).

## Sample Output

This repo provides a sample HTML at `sample/1093v-sample.html` which has been
created by converting a sample XML and the `github.css` of highlight.js.
You can either look at it at [sample/1093v-sample.html](sample/1093v-sample.html) or at

```bash
http://localhost:8080/exist/apps/code-view/sample/1093v-sample.html
```

after having started eXist-db and successfully having installed the Code Viewer.

Another place to see the Code Viewer in action is the TEI view of [Theodor Fontane's notebook C7](https://fontane-nb.dariah.eu/xml.html?id=/xml/data/16b00.xml) which uses the [Visual Studio style](https://github.com/highlightjs/highlight.js/blob/master/src/styles/vs.css) of [highlight.js](https://highlightjs.org/).

## Recent Changes and Semantic Versioning

The most recent changes in this module are tracked as a [changelog](https://keepachangelog.com/en/1.0.0/) in [repo.xml](https://gitlab.gwdg.de/SADE/code-viewer/blob/develop/repo.xmll). The versioning follows the convention of [Semantic Versioning](https://semver.org/).

## Contributing

Feel free to contribute to this repo by sending us a pull request! Please make sure you stick to the [RDD style guides](https://github.com/subugoe/rdd-technical-reference/blob/master/rdd-technical-reference.md) for XQuery.

## License

See  the [LICENSE](https://gitlab.gwdg.de/SADE/SADE/blob/develop/LICENSE) file.
