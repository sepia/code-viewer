xquery version "3.1";

(: This is the main test script for the module content/main.xqm.
 :
 : @version 1.0.0
 : @since 2.5.6
 : @author Michelle Weidling
 : @author Mathias Göbel
 : :)

module namespace cv-test="http://sepia.io/code-viewer/test";

import module namespace code-view="http://sepia.io/code-viewer/main";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace test="http://exist-db.org/xquery/xqsuite";
declare namespace xhtml="http://www.w3.org/1999/xhtml/";


(: Test to test the test :)
declare
    %test:name("FAIL")
    %test:args("<milestone/>")
    %test:assertFalse
    function cv-test:test-basic-000($node as element(*)) {
        false()
};


declare
    %test:name("Basic: no child nodes, no attributes")
    %test:args("<test/>")
    %test:assertXPath("$result[self::xhtml:pre]/xhtml:code/xhtml:span[@class=""hljs-tag""]/child::text()[1] = ""&lt;""")

    %test:args("<test/>")
    %test:assertXPath("$result//xhtml:span[@class=""hljs-tag""]/xhtml:span[@class=""hljs-name""]")

    %test:args("<test/>")
    %test:assertXPath("$result//xhtml:span[@class=""hljs-name""]/child::text() = ""test""")

    %test:args("<test/>")
    %test:assertXPath("$result//xhtml:span[@class=""hljs-tag""]/child::text()[last()] = ""/&gt;""")
    function cv-test:test-basic-001($node as element(*)) {
        code-view:main($node)
};


declare
    %test:name("Basic: no child nodes, with attribute(s)")
    %test:args("<test type=""class""/>")
    %test:assertXPath("$result//xhtml:span[@class=""hljs-attr""]/child::text()[1] = ""type""")

    %test:args("<test type=""class""/>")
    %test:assertXPath("$result//xhtml:span[@class=""hljs-string""]/child::text()[1][contains(., ""class"")]")

    %test:args("<test type=""class""/>")
    %test:assertXPath("$result//xhtml:span[@class=""hljs-name""]/following-sibling::text()[1] = "" """)

    function cv-test:test-basic-002($node as element(*)) {
        code-view:main($node)
};


declare
    %test:name("Basic: with child node(s)")
    %test:args("<test><childnode/></test>")
    %test:assertXPath("$result//xhtml:span[@class=""hljs-tag""]/xhtml:span[@class=""hljs-name""]/string() = ""childnode""")

    %test:args("<test><childnode/></test>")
    %test:assertXPath("$result//xhtml:span[@class=""hljs-name""][./string() = ""test""]/following::xhtml:span[@class=""hljs-name""][1]/descendant::text() = ""childnode""")
    function cv-test:test-basic-003($node as element(*)) {
        code-view:main($node)
};


declare
    %test:name("Comments: display")
    %test:args("<test><!-- test --></test>")
    %test:assertXPath("$result//xhtml:span[@class=""hljs-comment""]")

    %test:args("<test><!-- test --></test>")
    %test:assertXPath("$result//xhtml:span[@class=""hljs-comment""]/child::text() = ""&lt;!-- test --&gt;""")

    function cv-test:test-basic-004($node as element(*)) {
        code-view:main($node, true(), "hljs-", false())
};


declare
    %test:name("Comments: don't display")
    %test:args("<test><!-- test --></test>")
    %test:assertXPath("not($result//xhtml:span[@class=""hljs-comment""])")

    function cv-test:test-basic-005($node as element(*)) {
        code-view:main($node, false(), "hljs-", false())
};


declare
    %test:name("Prefix: change")
    %test:args("<test><!-- test --></test>")
    %test:assertXPath("$result//xhtml:span[@class=""another-prefix-tag""]")

    function cv-test:test-basic-006($node as element(*)) {
        code-view:main($node, false(), "another-prefix-", false())
};


declare
    %test:name("Indent: true")
    %test:args("<test>
        <childnode/>
    </test>")
    %test:assertXPath("$result//xhtml:span[@class=""hljs-tag""][1]/following::*[1][self::xhtml:span[@class=""hljs-tag""]]")

    %test:args("<test>
        <childnode/>
        </test>")
    %test:assertXPath("not($result//xhtml:span[@class=""hljs-tag""][2]/following::node()[1][matches(., ""[\s]{4}"")])")

    function cv-test:test-basic-007($node as element(*)) {
        code-view:main($node, false(), "hljs-", true())
};


declare
    %test:name("Indent: false")
    %test:args("<test>    <childnode/></test>")
    %test:assertXPath("$result//xhtml:span[@class=""hljs-tag""][1]/following::node()[1][matches(., ""[\s]{4}"")]")

    function cv-test:test-basic-008($node as element(*)) {
        code-view:main($node, false(), "hljs-", false())
};

(:~ hljs xml example 1
 : @see https://highlightjs.org/?snippet=1&style=14 :)
declare
    %test:name("hljs example")
    %test:args('<title>Title</title>')
    %test:assertTrue
    %test:assertXPath("$result//xhtml:span[matches(@class, 'hljs')]")
    function cv-test:test-basic-009($node as element(title)) {
        code-view:main($node)
};
